class CreateUserBindings < ActiveRecord::Migration
  def change
    create_table :user_bindings do |t|
      t.integer :user_id
      t.integer :party_id

      t.timestamps
    end
  end
end
