class CreateFengScores < ActiveRecord::Migration
  def change
    create_table :feng_scores do |t|
      t.integer :user_id
      t.integer :party_id
      t.integer :xshy
      t.text :xshy_detail
      t.integer :jxyx
      t.text :jxyx_detail
      t.integer :astj
      t.text :astj_detail
      t.integer :wchy
      t.text :wchy_detail
      t.integer :zxhy
      t.text :zxhy_detail
      t.integer :zjsf
      t.text :zjsf_detail
      t.integer :dzyl
      t.text :dzyl_detail
      t.integer :rxxh
      t.text :rxxh_detail
      t.integer :zsxg
      t.text :zsxg_detail
      t.integer :zsgz
      t.text :zsgz_detail
      t.integer :rzzx
      t.text :rzzx_detail
      t.integer :xshd
      t.text :xshd_detail

      t.timestamps
    end
  end
end
