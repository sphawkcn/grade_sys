class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|
      t.string :username
      t.string :password
      t.integer :rule
      t.integer :state

      t.timestamps
    end
  end
end
