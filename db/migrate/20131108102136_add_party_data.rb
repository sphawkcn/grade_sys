class AddPartyData < ActiveRecord::Migration
  def change
  	add_parites_data("./data/parties.txt")
  end

  def add_parites_data( file_path)
  	file= File.open( file_path)
  	file.each_line do |line|
  		line=line.chomp
  		party= Party.new( name: line.to_s)
  		party.save
  	end
  end
end
