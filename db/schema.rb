# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20131109135303) do

  create_table "feng_scores", force: true do |t|
    t.integer  "user_id"
    t.integer  "party_id"
    t.integer  "xshy"
    t.text     "xshy_detail"
    t.integer  "jxyx"
    t.text     "jxyx_detail"
    t.integer  "astj"
    t.text     "astj_detail"
    t.integer  "wchy"
    t.text     "wchy_detail"
    t.integer  "zxhy"
    t.text     "zxhy_detail"
    t.integer  "zjsf"
    t.text     "zjsf_detail"
    t.integer  "dzyl"
    t.text     "dzyl_detail"
    t.integer  "rxxh"
    t.text     "rxxh_detail"
    t.integer  "zsxg"
    t.text     "zsxg_detail"
    t.integer  "zsgz"
    t.text     "zsgz_detail"
    t.integer  "rzzx"
    t.text     "rzzx_detail"
    t.integer  "xshd"
    t.text     "xshd_detail"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "messages", force: true do |t|
    t.integer  "user"
    t.text     "content"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "parties", force: true do |t|
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "user_bindings", force: true do |t|
    t.integer  "user_id"
    t.integer  "party_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "users", force: true do |t|
    t.string   "username"
    t.string   "password"
    t.integer  "rule"
    t.integer  "state"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

end
