class GradeController < ApplicationController

  layout 'login', :only=>[:index]

  def index
  	@user= User.new
  end

  def enter
	  	username=params[:user][:username]
	  	user=User.find_by_username(username)
	  	if user
	  		if user.password== params[:user][:password]
	  			cookies[:id]= user.id
	  			case user.state
	  			when 0
	  				redirect_to :action=>'feng_guan_zhuan_yuan'
	  			when 1
	  				render :action=>'zhuan_ke_feng_hui'
	  			else
	  				render :action=>' error_state'
	  		    end
	  		else
		  		redirect_to root_path
	  		end
	  	else
	  		redirect_to root_path
	  	end
  end

  def feng_guan_zhuan_yuan
  	@user= User.find(cookies[:id])
    @feng_score= FengScore.new
  end

  def submit_fgzy
    @score= FengScore.new(feng_score_params)
    @score.save
    redirect_to :action=>'feng_guan_zhuan_yuan'
  end

  def zhuan_ke_feng_hui
  	@user= User.find( cookies[:id])
  end

  def error_state

  end

  def add_options
    bindings= UserBinding.where("user_id= #{cookies[:id]}" )
    parties=[]
    bindings.each do |b|
      parties<< Party.find( b.party_id)
    end
  	render :json=> parties
  end


  def leave_message
    message= params[:message]
    id= cookies[:id]

    new_msg= Message.new( content: message, user: id)
    new_msg.save
    render :json=>1

  end

  def change_right_bar
    data=[]
    done=[]
    done_count=0
    undone=[]
    undone_count=0
    party= Party.find( params[:id]).name
    data[0]= party

    done_temp= UserBinding.where(" party_id=#{ params[:id]}")
    done_temp.each do |d|
      if ( d.user.state==1 && d.user.rule== 0)
        done<< d.user.username
        done_count= done_count+1
      elsif ( d.user.state==1 && d.user.rule ==1)
        undone<< d.user.username
        undone_count =undone_count+1
      end
    end

    data[1]= done_count
    data[2]= done
    data[3]= undone_count
    data[4]= undone
    if ( done_count==0 && undone_count==0)
      data[5]=0
    else
      data[5]= 100*( done_count.to_f/( done_count+ undone_count))
    end

    render :json=> data
  end

  def change_header
    id= cookies[:id]
    user= User.find( id).username
    party= Party.find( params[:id]).name
    data=[]
    data[0]=user
    data[1]=party
    render :json=> data
  end

  private

  def feng_score_params
    params.require(:feng_score).permit(:user_id, :party_id,
                                       :xshy, :xshy_detail,
                                       :jxyx, :jxyx_detail,
                                       :astj, :astj_detail,
                                       :wchy, :wchy_detail,
                                       :zxhy, :zxhy_detail,
                                       :zjsf, :zjsf_detail,
                                       :dzyl, :dzyl_detail,
                                       :rxxh, :rxxh_detail,
                                       :zsxg, :zsxg_detail,
                                       :zsgz, :zsgz_detail,
                                       :rzzx, :rzzx_detail,
                                       :xshd, :xshd_detail)
  end
=begin
  def fgzy_update_attr
    flag  = params[:flag]
    value = params[:value]
    detail= params[:detail]
    party = params[:party]
    id= cookies[:id]

    score= FengScore.find(:first, :conditions=>[" user_id= ? and party_id=?", id, party])
    if !score
      score=FengScore.new( user_id: id, party_id: party)
      score.save
      score= FengScore.find(:first, :conditions=>[" user_id= ? and party_id=?", id, party])
    end

    case flag
    when "1"
      score.update_attributes( :xshy=> value, :xshy_detail=> detail)
    when "2"
      score.update_attributes( :jxyx=> value, :jxyx_detail=> detail)
    when "3"
      score.update_attributes( :astj=> value, :astj_detail=> detail)
    when "4"
      score.update_attributes( :wchy=> value, :wchy_detail=> detail)
    when "5"
      score.update_attributes( :zxhy=> value, :zxhy_detail=> detail)
    when "6"
      score.update_attributes( :zjsf=> value, :zjsf_detail=> detail)
    when "7"
      score.update_attributes( :dzyl=> value, :dzyl_detail=> detail)
    when "8"
      score.update_attributes( :rxxh=> value, :rxxh_detail=> detail)
    when "9"
      score.update_attributes( :zsxg=> value, :zsxg_detail=> detail)
    when "10"
      score.update_attributes( :zsgz=> value, :zsgz_detail=> detail)
    when "11"
      score.update_attributes( :rzzx=> value, :rzzx_detail=> detail)
    when "12"
      score.update_attributes( :xshd=> value, :xshd_detail=> detail)
    else
    end
    render :json=>1
  end

=end
end
